﻿#include <GL/glut.h>
#include "glext.h"
#include <stdlib.h>
#include <stdio.h>
#include "colors.h"
#include "targa.h"
#include <iostream>

PFNGLWINDOWPOS2IPROC glWindowPos2i = NULL;


enum
{
	TEXTURE_COMPRESSION_FASTEST,
	TEXTURE_COMPRESSION_DONT_CARE,
	TEXTURE_COMPRESSION_NICEST,
	TEXTURE_LENA,
	TEXTURE_LENA_UNC,
	TEXTURE_LENA_GRAY,
	TEXTURE_LENA_GRAY_UNC,
	FULL_WINDOW,
	ASPECT_1_1,
	EXIT
};


int aspect = FULL_WINDOW;


#ifdef near
#undef near
#endif
#ifdef far
#undef far
#endif


const GLdouble left = -2.0;
const GLdouble right = 2.0;
const GLdouble bottom = -2.0;
const GLdouble top = 2.0;
const GLdouble near = 3.0;
const GLdouble far = 7.0;


GLuint LENA, LENA_UNC, LENA_GRAY, LENA_GRAY_UNC;


GLuint texture;


GLint texture_compression_hint = GL_DONT_CARE;


void DrawString(GLint x, GLint y, char *string)
{
	if (glWindowPos2i == nullptr)
		return;

	glWindowPos2i(x, y);


	int len = strlen(string);
	for (int i = 0; i < len; i++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, string[i]);
}


void DisplayScene()
{

	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -(near + far) / 2);
	glTranslatef(0.0, 1.0, 0.0);
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex2f(-1.5, -1.5);
	glTexCoord2f(0.0, 1.0);
	glVertex2f(-1.5, 1.5);
	glTexCoord2f(1.0, 1.0);
	glVertex2f(1.5, 1.5);
	glTexCoord2f(1.0, 0.0);
	glVertex2f(1.5, -1.5);
	glEnd();
	glDisable(GL_TEXTURE_2D);


	char string[200];
	GLint var;
	glColor3fv(Black);


	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED, &var);
	if (var == GL_FALSE)
		sprintf_s(string, "GL_TEXTURE_COMPRESSED = GL_FALSE");
	else
		sprintf_s(string, "GL_TEXTURE_COMPRESSED = GL_TRUE");
	DrawString(2, 2, string);


	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &var);
	sprintf_s(string, "GL_TEXTURE_COMPRESSED_IMAGE_SIZE = %i", var);
	DrawString(2, 16, string);


	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &var);
	switch (var)
	{

	case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGB_S3TC_DXT1_EXT");
		break;
	case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT");
		break;
	case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT");
		break;
	case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT");
		break;


	case GL_COMPRESSED_RGB_FXT1_3DFX:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGB_FXT1_3DFX");
		break;
	case GL_COMPRESSED_RGBA_FXT1_3DFX:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGBA_FXT1_3DFX");
		break;


	case 0x8837:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_COMPRESSED_RGB_3DC_ATI");
		break;


	case GL_RED:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_RED");
		break;
	case GL_GREEN:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_GREEN");
		break;
	case GL_BLUE:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_BLUE");
		break;
	case GL_ALPHA:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_ALPHA");
		break;
	case GL_RGB:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_RGB");
		break;
	case GL_RGBA:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_RGBA");
		break;
	case GL_LUMINANCE:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_LUMINANCE");
		break;
	case GL_LUMINANCE_ALPHA:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = GL_LUMINANCE_ALPHA");
		break;


	default:
		sprintf_s(string, "GL_TEXTURE_INTERNAL_FORMAT = nieznany");
		break;
	}
	DrawString(2, 30, string);


	glGetIntegerv(GL_TEXTURE_COMPRESSION_HINT, &var);
	switch (var)
	{
	case GL_FASTEST:
		sprintf_s(string, "GL_TEXTURE_COMPRESSION_HINT = GL_FASTEST");
		break;
	case GL_DONT_CARE:
		sprintf_s(string, "GL_TEXTURE_COMPRESSION_HINT = GL_DONT_CARE");
		break;
	case GL_NICEST:
		sprintf_s(string, "GL_TEXTURE_COMPRESSION_HINT = GL_NICEST");
		break;
	}
	DrawString(2, 44, string);


	glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, &var);
	sprintf_s(string, "GL_NUM_COMPRESSED_TEXTURE_FORMATS = %i", var);


	GLint formats[256];
	glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, formats);
	for (int i = 0; i < var; i++)
	{
		switch (formats[i])
		{

		case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
			sprintf_s(string, "GL_COMPRESSED_RGB_S3TC_DXT1_EXT");
			break;
		case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
			sprintf_s(string, "GL_COMPRESSED_RGBA_S3TC_DXT1_EXT");
			break;
		case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
			sprintf_s(string, "GL_COMPRESSED_RGBA_S3TC_DXT3_EXT");
			break;
		case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
			sprintf_s(string, "GL_COMPRESSED_RGBA_S3TC_DXT5_EXT");
			break;


		case GL_COMPRESSED_RGB_FXT1_3DFX:
			sprintf_s(string, "GL_COMPRESSED_RGB_FXT1_3DFX");
			break;
		case GL_COMPRESSED_RGBA_FXT1_3DFX:
			sprintf_s(string, "GL_COMPRESSED_RGBA_FXT1_3DFX");
			break;



		case 0x8837:
			sprintf_s(string, "GL_COMPRESSED_RGB_3DC_ATI");
			break;


		default:
			sprintf_s(string, "Format nieznany (0x%X)", formats[i]);
			break;
		}
		DrawString(2, 70 + 14 * i, string);
	}


	glFlush();


	glutSwapBuffers();
}


void Reshape(int width, int height)
{

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (aspect == ASPECT_1_1)
	{
		if (width < height && width > 0)
			glFrustum(left, right, bottom*height / width, top*height / width, near, far);
		else if (width >= height && height > 0)
			glFrustum(left*width / height, right*width / height, bottom, top, near, far);
	}
	else
		glFrustum(left, right, bottom, top, near, far);


	DisplayScene();
}


void GenerateTextures()
{

	GLsizei width, height;
	GLenum format, type;
	GLvoid *pixels;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	glHint(GL_TEXTURE_COMPRESSION_HINT, texture_compression_hint);


	GLboolean error = load_targa("white_skin_man_face_2_20141210_1200773164.tga", width, height, format, type, pixels);


	if (error == GL_FALSE)
	{
		printf("Niepoprawny odczyt pliku white_skin_man_face_2_20141210_1200773164.tga");
		exit(0);
	}


	glGenTextures(1, &LENA);
	glBindTexture(GL_TEXTURE_2D, LENA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB, width, height, 0, format, type, pixels);
	glGenTextures(1, &LENA_UNC);
	glBindTexture(GL_TEXTURE_2D, LENA_UNC);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, type, pixels);
	delete[](unsigned char*)pixels;


	error = load_targa("white_skin_man_face_2_20141210_1200773164.tga", width, height, format, type, pixels);


	if (error == GL_FALSE)
	{
		printf("Niepoprawny odczyt pliku white_skin_man_face_2_20141210_1200773164.tga");
		exit(0);
	}


	glGenTextures(1, &LENA_GRAY);
	glBindTexture(GL_TEXTURE_2D, LENA_GRAY);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_LUMINANCE, width, height, 0, format, type, pixels);
	glGenTextures(1, &LENA_GRAY_UNC);
	glBindTexture(GL_TEXTURE_2D, LENA_GRAY_UNC);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, format, type, pixels);
	delete[](unsigned char*)pixels;

	texture = LENA;
}


void Menu(int value)
{
	GLint tmp_texture;
	switch (value)
	{

	case TEXTURE_COMPRESSION_FASTEST:
	{
		texture_compression_hint = GL_FASTEST;
		tmp_texture = texture;
		GenerateTextures();
		texture = tmp_texture;
		DisplayScene();
	}
	break;


	case TEXTURE_COMPRESSION_DONT_CARE:
	{
		texture_compression_hint = GL_DONT_CARE;
		tmp_texture = texture;
		GenerateTextures();
		texture = tmp_texture;
		DisplayScene();
	}
	break;


	case TEXTURE_COMPRESSION_NICEST:
	{
		texture_compression_hint = GL_NICEST;
		tmp_texture = texture;
		GenerateTextures();
		texture = tmp_texture;
		DisplayScene();
	}
	break;


	case TEXTURE_LENA:
	{
		texture = LENA;
		DisplayScene();
	}
	break;


	case TEXTURE_LENA_UNC:
	{
		texture = LENA_UNC;
		DisplayScene();
	}
	break;


	case TEXTURE_LENA_GRAY:
	{
		texture = LENA_GRAY;
		DisplayScene();
	}
	break;


	case TEXTURE_LENA_GRAY_UNC:
	{
		texture = LENA_GRAY_UNC;
		DisplayScene();
	}
	break;


	case FULL_WINDOW:
	{
		aspect = FULL_WINDOW;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	}
	break;


	case ASPECT_1_1:
	{
		aspect = ASPECT_1_1;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	}
	break;


	case EXIT:
		exit(0);
	}
}


void ExtensionSetup()
{

	const char *version = (char*)glGetString(GL_VERSION);


	int major = 0, minor = 0;
	if (sscanf_s(version, "%d.%d", &major, &minor) != 2)
	{
#ifdef WIN32
		printf("Błędny format wersji OpenGL\n");
#else

		printf("Bledny format wersji OpenGL\n");
#endif

		exit(0);
	}


	if (major > 1 || minor >= 4)
	{

		glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress("glWindowPos2i");
	}
	else

		if (glutExtensionSupported("GL_ARB_window_pos"))
		{

			glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress
			("glWindowPos2iARB");
		}
		else
		{
			printf("Brak rozszerzenia ARB_window_pos!\n");
			exit(0);
		}



	if (!(major > 1 || minor >= 3) &&
		!glutExtensionSupported("GL_ARB_texture_compression"))
	{
		printf("Brak rozszerzenia GL_ARB_texture_compression!\n");
		exit(0);
	}
}

int main(int argc, char *argv[])
{

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(550, 550);
	glutCreateWindow("Kompresja tekstur");
	glutDisplayFunc(DisplayScene);
	glutReshapeFunc(Reshape);

	int MenuTexture = glutCreateMenu(Menu);
	glutAddMenuEntry("lena.tga (kompresja)", TEXTURE_LENA);
	glutAddMenuEntry("lena.tga (bez kompresji)", TEXTURE_LENA_UNC);
	glutAddMenuEntry("lena_gray.tga (kompresja)", TEXTURE_LENA_GRAY);
	glutAddMenuEntry("lena_gray.tga (bez kompresji)", TEXTURE_LENA_GRAY_UNC);


	int MenuTextureCompressionHint = glutCreateMenu(Menu);
	glutAddMenuEntry("GL_FASTEST", TEXTURE_COMPRESSION_FASTEST);
	glutAddMenuEntry("GL_DONT_CARE", TEXTURE_COMPRESSION_DONT_CARE);
	glutAddMenuEntry("GL_NICEST", TEXTURE_COMPRESSION_NICEST);


	int MenuAspect = glutCreateMenu(Menu);
#ifdef WIN32

	glutAddMenuEntry("Aspekt obrazu - całe okno", FULL_WINDOW);
#else

	glutAddMenuEntry("Aspekt obrazu - cale okno", FULL_WINDOW);
#endif

	glutAddMenuEntry("Aspekt obrazu 1:1", ASPECT_1_1);

	glutCreateMenu(Menu);
	glutAddSubMenu("Tekstura", MenuTexture);
	glutAddSubMenu("GL_TEXTURE_COMPRESSION_HINT", MenuTextureCompressionHint);
	glutAddSubMenu("Aspekt obrazu", MenuAspect);

#ifdef WIN32

	glutAddMenuEntry("Wyjście", EXIT);
#else

	glutAddMenuEntry("Wyjscie", EXIT);
#endif

	glutAttachMenu(GLUT_RIGHT_BUTTON);
	GenerateTextures();
	ExtensionSetup();
	system("PAUSE");
	glutMainLoop();
	int a;
	std::cin >> a;

	return 0;
}
