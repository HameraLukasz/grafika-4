﻿
#include <GL/glut.h>
#include "glext.h"
#include <stdlib.h>
#include <stdio.h>
#include "colors.h"
#include "targa.h"

PFNGLWINDOWPOS2IPROC glWindowPos2i = NULL;

enum
{
	FULL_WINDOW,  
	ASPECT_1_1,   
	EXIT          
};

int aspect = FULL_WINDOW;

#ifdef near
#undef near
#endif
#ifdef far
#undef far
#endif

const GLdouble left = -2.0;
const GLdouble right = 2.0;
const GLdouble bottom = -2.0;
const GLdouble top = 2.0;
const GLdouble near = 3.0;
const GLdouble far = 7.0;

GLfloat rotatex = 0.0;
GLfloat rotatey = 0.0;

int button_state = GLUT_UP;

int button_x, button_y;

GLfloat scale = 1.5;

GLuint GRASS, TREE;

GLint GRASS_LIST, TREE_LIST;

bool alpha_test = true;

bool blend = false;

void DrawString(GLint x, GLint y, char *string)
{
	if (glWindowPos2i == nullptr)
		return;
	glWindowPos2i(x, y);

	int len = strlen(string);
	for (int i = 0; i < len; i++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, string[i]);
}

void DisplayScene()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glTranslatef(0.0, 0.0, -(near + far) / 2);

	glRotatef(rotatex, 1.0, 0.0, 0.0);
	glRotatef(rotatey, 0.0, 1.0, 0.0);

	glScalef(scale, scale, scale);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	if (alpha_test == GL_TRUE)
	{
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.5);
	}
	else
		glDisable(GL_ALPHA_TEST);

	if (blend == GL_TRUE)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
		glDisable(GL_BLEND);

	glBindTexture(GL_TEXTURE_2D, GRASS);
	glPushMatrix();
	glTranslatef(0.0, -0.5, 0.0);
	glCallList(GRASS_LIST);
	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, TREE);
	glPushMatrix();
	glTranslatef(-1.5, 0.0, 0.0);
	glScalef(0.5, 0.5, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glTranslatef(1.0, 0.0, 0.5);
	glCallList(TREE_LIST);
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	char string[200];
	GLboolean var;
	glColor3fv(Black);

	glGetBooleanv(GL_ALPHA_TEST, &var);
	if (var == GL_TRUE)
		strcpy_s(string, "GL_ALPHA_TEST = GL_TRUE");
	else
		strcpy_s(string, "GL_ALPHA_TEST = GL_FALSE");
	DrawString(2, glutGet(GLUT_WINDOW_HEIGHT) - 17, string);

	glGetBooleanv(GL_BLEND, &var);
	if (var == GL_TRUE)
		strcpy_s(string, "GL_BLEND = GL_TRUE");
	else
		strcpy_s(string, "GL_BLEND = GL_FALSE");
	DrawString(2, glutGet(GLUT_WINDOW_HEIGHT) - 33, string);

	glFlush();

	glutSwapBuffers();
}

void Reshape(int width, int height)
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	if (aspect == ASPECT_1_1)
	{

		if (width < height && width > 0)
			glFrustum(left, right, bottom*height / width, top*height / width, near, far);
		else

	
			if (width >= height && height > 0)
				glFrustum(left*width / height, right*width / height, bottom, top, near, far);
	}
	else
		glFrustum(left, right, bottom, top, near, far);

	DisplayScene();
}

void Keyboard(unsigned char key, int x, int y)
{
	if (key == '+')
		scale += 0.05;
	else


		if (key == '-' && scale > 0.05)
			scale -= 0.05;

	DisplayScene();
}

void MouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{

		button_state = state;


		if (state == GLUT_DOWN)
		{
			button_x = x;
			button_y = y;
		}
	}
}

void MouseMotion(int x, int y)
{
	if (button_state == GLUT_DOWN)
	{
		rotatey += 30 * (right - left) / glutGet(GLUT_WINDOW_WIDTH) * (x - button_x);
		button_x = x;
		rotatex -= 30 * (top - bottom) / glutGet(GLUT_WINDOW_HEIGHT) * (button_y - y);
		button_y = y;
		glutPostRedisplay();
	}
}

void GenerateTextures()
{
	GLsizei width, height;
	GLenum format, type;
	GLvoid *pixels;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	GLboolean error = load_targa("wildtextures-deep-grass.tga", width, height, format, type, pixels);

	if (error == GL_FALSE)
	{
		printf("Niepoprawny odczyt pliku grass_color.tga");
		exit(0);
	}

	glGenTextures(1, &GRASS);

	glBindTexture(GL_TEXTURE_2D, GRASS);

	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, type, pixels);

	delete[](unsigned char*)pixels;

	error = load_targa("pine_tree_big_20150403_2028379675.tga", width, height, format, type, pixels);

	if (error == GL_FALSE)
	{
		printf("Niepoprawny odczyt pliku branch.tga");
		exit(0);
	}

	glGenTextures(1, &TREE);

	glBindTexture(GL_TEXTURE_2D, TREE);

	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, type, pixels);

	delete[](unsigned char*)pixels;
}

void Menu(int value)
{
	switch (value)
	{

	case GL_ALPHA_TEST:
		alpha_test = !alpha_test;
		DisplayScene();
		break;


	case GL_BLEND:
		blend = !blend;
		DisplayScene();
		break;


	case FULL_WINDOW:
		aspect = FULL_WINDOW;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;


	case ASPECT_1_1:
		aspect = ASPECT_1_1;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;


	case EXIT:
		exit(0);
	}
}

void GenerateDisplayLists()
{
	GRASS_LIST = glGenLists(1);

	glNewList(GRASS_LIST, GL_COMPILE);

	glBegin(GL_QUADS);
	glTexCoord2f(16.0, 0.0);
	glVertex3f(-8.0, 0.0, -8.0);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-8.0, 0.0, 8.0);
	glTexCoord2f(0.0, 16.0);
	glVertex3f(8.0, 0.0, 8.0);
	glTexCoord2f(16.0, 16.0);
	glVertex3f(8.0, 0.0, -8.0);
	glEnd();

	glEndList();

	TREE_LIST = glGenLists(1);

	glNewList(TREE_LIST, GL_COMPILE);

	glBegin(GL_TRIANGLES);

	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0, -1.0, 0.0);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(1.0, -1.0, 0.0);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(1.0, 1.0, 0.0);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0, -1.0, 0.0);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(1.0, 1.0, 0.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-1.0, 1.0, 0.0);

	glEnd();

	glEndList();
}

void ExtensionSetup()
{
	const char *version = (char*)glGetString(GL_VERSION);

	int major = 0, minor = 0;
	if (sscanf_s(version, "%d.%d", &major, &minor) != 2)
	{
#ifdef WIN32
		printf("Błędny format wersji OpenGL\n");
#else

		printf("Bledny format wersji OpenGL\n");
#endif

		exit(0);
	}

	if (!(major > 1 || minor >= 4) &&
		!glutExtensionSupported("GL_SGIS_generate_mipmap"))
	{
		printf("Brak rozszerzenia GL_SGIS_generate_mipmap!\n");
		exit(0);
	}

	if (major > 1 || minor >= 4)
	{

		glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress("glWindowPos2i");
	}
	else

		if (glutExtensionSupported("GL_ARB_window_pos"))
		{
	
			glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress
			("glWindowPos2iARB");
		}
		else
		{
			printf("Brak rozszerzenia ARB_window_pos!\n");
			exit(0);
		}
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	glutInitWindowSize(500, 500);

	glutCreateWindow("Tekstura alfa");

	glutDisplayFunc(DisplayScene);

	glutReshapeFunc(Reshape);

	glutKeyboardFunc(Keyboard);

	glutMouseFunc(MouseButton);

	glutMotionFunc(MouseMotion);

	int MenuAspect = glutCreateMenu(Menu);
#ifdef WIN32

	glutAddMenuEntry("Aspekt obrazu - całe okno", FULL_WINDOW);
#else

	glutAddMenuEntry("Aspekt obrazu - cale okno", FULL_WINDOW);
#endif

	glutAddMenuEntry("Aspekt obrazu 1:1", ASPECT_1_1);

	glutCreateMenu(Menu);

#ifdef WIN32

	glutAddMenuEntry("Test kanału alfa: włącz/wyłącz", GL_ALPHA_TEST);
	glutAddMenuEntry("Mieszanie kolorów: włącz/wyłącz", GL_BLEND);
	glutAddSubMenu("Aspekt obrazu", MenuAspect);
	glutAddMenuEntry("Wyjście", EXIT);
#else

	glutAddMenuEntry("Test kanalu alfa: wlacz/wylacz", GL_ALPHA_TEST);
	glutAddMenuEntry("Mieszanie kolorow: wlacz/wylacz", GL_BLEND);
	glutAddSubMenu("Aspekt obrazu", MenuAspect);
	glutAddMenuEntry("Wyjscie", EXIT);
#endif

	glutAttachMenu(GLUT_RIGHT_BUTTON);

	GenerateTextures();

	ExtensionSetup();

	GenerateDisplayLists();

	glutMainLoop();
	return 0;
}
